library(dplyr)
library(ggplot2)
library(see)
library(ggrepel)
library(gghalves)

all_stars_2001_2020 <- read.csv("data/worst_all_star_2000_2020.csv",
                                stringsAsFactors = FALSE)

all_stars_2001_2020 <-
  all_stars_2001_2020 %>%
  group_by(year) %>%
  mutate(ws_scale_year = (ws - mean(ws)) / sd(ws),
         ws_48_scale_year = (ws_48 - mean(ws_48)) / sd(ws_48),
         vorp_scale_year = (vorp - mean(vorp)) / sd(vorp),
         ortg_scale_year = (ortg - mean(ortg)) / sd(ortg),
         drtg_scale_year = ((drtg - mean(drtg)) / sd(drtg)) * -1,
         net_rtg_scale_year = (net_rtg - mean(net_rtg)) / sd(net_rtg)) %>%
  ungroup() %>%
  mutate(year_divergence = ws_scale_year + ws_48_scale_year +
           vorp_scale_year + ortg_scale_year + drtg_scale_year + 
           net_rtg_scale_year,
         voted_starter = ifelse(voted_starter == "yes", 1, 0),
         all_star_starter = ifelse(all_star_starter == "yes", 1, 0),
         injured = ifelse(injured == "yes", 1, 0),
         replacement = ifelse(replacement == "yes", 1, 0),
         last_season = ifelse(last_season == "yes", 1, 0),
         selection = case_when(voted_starter == 1 ~ "Voted Starter",
                               replacement == 1 ~ "Commisioner's Replacement",
                               TRUE ~ "Coach's Vote"))

m0 <- lm(year_divergence ~ selection * n_selection,
         data = all_stars_2001_2020)         
summary(m0)

ggplot(all_stars_2001_2020, aes(x = selection, y = whole_sample_divergence,
                                color = year)) +
  geom_half_violin() +
  geom_half_point() +
  # geom_jitter(shape = 16, width = 0.35) +
  # geom_boxplot(alpha = 0.3) +
  scale_color_viridis_c() +
  xlab("Selection Method") +
  ylab("Z-Score") +
  fischeR::theme_saf()

ggplot(all_stars_2001_2020, aes(x = n_selection, y = whole_sample_divergence,
                                color = selection)) +
  geom_point() +
  geom_smooth() +
  facet_wrap(~selection) +
  fischeR::theme_saf()

new_data <-
  data.frame(n_selection = rep(seq(1:max(all_stars_2001_2020$n_selection)), 3),
             selection = c(rep("Coach's Vote", 18), 
                           rep("Commisioner's Replacement", 18),
                           rep("Voted Starter", 18)))
pred_divergence <- predict(m0, newdata = new_data, interval = "confidence")
new_data$year_divergence <- pred_divergence[, 1]
new_data$year_divergence_lwr <- pred_divergence[, 2]
new_data$year_divergence_upr <- pred_divergence[, 3]

ggplot(new_data, aes(x = n_selection, y = year_divergence,
                     lty = selection, fill = selection)) +
  geom_ribbon(aes(ymin = year_divergence_lwr, ymax = year_divergence_upr),
              alpha = 0.3) +
  geom_line() +
  scale_fill_discrete("Selection Process") +
  scale_linetype_discrete("Selection Process") +
  xlab("Selection Number") +
  ylab("Within Year Z-Score") +
  fischeR::theme_saf()

ggplot(all_stars_2001_2020, aes(x = year, y = year_divergence)) +
  geom_point() +
  scale_x_continuous(breaks = c(2001:2020)) +
  geom_hline(yintercept = 0) +
  geom_smooth() +
  xlab("Year") +
  ylab("Total Score") +
  fischeR::theme_saf()

first_all_star <-
  all_stars_2001_2020 %>%
  filter(n_selection == 1,
         year != 2020)

n_selection_df <-
  all_stars_2001_2020 %>%
  # filter(n_selection >= 2) %>%
  filter(name %in% first_all_star$name) %>%
  group_by(n_selection) %>%
  summarise(Count = n(),
            last_season = sum(last_season))

# n_selection_df <-
#   rbind(c(1, length(unique(all_stars_2001_2020$name))),
#         n_selection_df)

n_selection_df <-
  n_selection_df %>%
  mutate(
    standard = ifelse(n_selection == 1, 1, Count / Count[1]),
      conditional = (Count / Count[1]) / 
      ((lag(Count) - lag(last_season)) / Count[1])
         ) %>%
  select(-last_season)

n_selection_df_2 <-
  n_selection_df %>%
  mutate(change_standard = standard - lag(standard),
         change_conditional = conditional - lag(conditional)) %>%
  select(-c(standard, conditional))

n_selection_df <-
  n_selection_df %>%
  tidyr::pivot_longer(-c(n_selection, Count), names_to = "type", 
                      values_to = "prob")

n_selection_df_2 <-
  n_selection_df_2 %>%
  tidyr::pivot_longer(-c(n_selection, Count), names_to = "type", 
                      values_to = "change")

ggplot(n_selection_df, aes(x = n_selection, y = prob, group = type)) +
  geom_point(aes(shape = type, size = Count)) +
  geom_line(aes(lty = type)) +
  scale_x_continuous(breaks = c(1:18)) +
  scale_shape_discrete("Type of Probability", solid = FALSE) +
  scale_size_continuous("# Players Earning Nth Selection", 
                        breaks = c(1, 25, 50, 75, 100)) +
  scale_linetype_discrete("Type of Probability") +
  xlab("Nth Selection") +
  ylab("Probability of Nth All Star Selection") +
  labs(caption = 
         "Based on players making first All-Star Game between 2001 and 2019") +
  fischeR::theme_saf() +
  theme(legend.direction = "horizontal",
        legend.position = "bottom")

ggplot(n_selection_df_2, aes(x = n_selection, y = change, group = type)) +
  geom_point(aes(shape = type), size = 2) +
  geom_line(aes(lty = type)) +
  geom_hline(yintercept = 0) +
  scale_x_continuous(breaks = c(1:17)) +
  scale_shape_discrete("Type of Probability", solid = FALSE) +
  scale_linetype_discrete("Type of Probability") +
  xlab("Selection Number") +
  ylab("Change in Probability of All Star Selection") +
  labs(caption = 
         "Based on players making first All-Star Game between 2001 and 2020") +
  fischeR::theme_saf() +
  theme(legend.direction = "horizontal",
        legend.position = "bottom")

ggplot(n_selection_df, aes(x = n_selection, y = Count)) +
  geom_point() +
  geom_line() +
  scale_x_continuous(breaks = c(1:17)) +
  xlab("Selection Number") +
  fischeR::theme_saf()
