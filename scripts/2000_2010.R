library(dplyr)
library(ggplot2)

all_stars_2001_2010 <- read.csv("data/worst_all_star_2000_2010.csv",
                                stringsAsFactors = FALSE)

all_stars_2001_2010 <-
  all_stars_2001_2010 %>%
  group_by(year) %>%
  mutate(ws_scale_year = (ws - mean(ws)) / sd(ws),
         ws_48_scale_year = (ws_48 - mean(ws_48)) / sd(ws_48),
         vorp_scale_year = (vorp - mean(vorp)) / sd(vorp),
         ortg_scale_year = (ortg - mean(ortg)) / sd(ortg),
         drtg_scale_year = ((drtg - mean(drtg)) / sd(drtg)) * -1,
         net_rtg_scale_year = (net_rtg - mean(net_rtg)) / sd(net_rtg)) %>%
  ungroup() %>%
  mutate(year_divergence = ws_scale_year + ws_48_scale_year +
           vorp_scale_year + ortg_scale_year + drtg_scale_year + 
           net_rtg_scale_year,
         voted_starter = ifelse(voted_starter == "yes", 1, 0),
         all_star_starter = ifelse(all_star_starter == "yes", 1, 0),
         injured = ifelse(injured == "yes", 1, 0),
         replacment = ifelse(replacment == "yes", 1, 0),
         selection = case_when(voted_starter == 1 ~ "Voted Starter",
                               replacment == 1 ~ "Commisioner's Replacement",
                               TRUE ~ "Coach's Vote"))

m0 <- lm(year_divergence ~ selection * n_selection,
         data = all_stars_2001_2010)         
summary(m0)

ggplot(all_stars_2001_2010, aes(x = selection, y = year_divergence)) +
  geom_jitter(shape = 21, width = 0.35) +
  geom_boxplot(alpha = 0.3) +
  xlab("Selection Method") +
  ylab("Within Year Z-Score") +
  fischeR::theme_saf()

ggplot(all_stars_2001_2010, aes(x = n_selection, y = year_divergence,
                                color = selection)) +
  geom_point() +
  geom_smooth() +
  facet_wrap(~selection) +
  fischeR::theme_saf()

new_data <-
  data.frame(n_selection = rep(seq(1:max(all_stars_2001_2010$n_selection)), 3),
             selection = c(rep("Coach's Vote", 15), 
                           rep("Commisioner's Replacement", 15),
                           rep("Voted Starter", 15)))
pred_divergence <- predict(m0, newdata = new_data, interval = "confidence")
new_data$year_divergence <- pred_divergence[, 1]
new_data$year_divergence_lwr <- pred_divergence[, 2]
new_data$year_divergence_upr <- pred_divergence[, 3]

ggplot(new_data, aes(x = n_selection, y = year_divergence,
                                lty = selection, fill = selection)) +
  geom_ribbon(aes(ymin = year_divergence_lwr, ymax = year_divergence_upr),
              alpha = 0.3) +
  geom_line() +
  scale_fill_discrete("Selection Process") +
  scale_linetype_discrete("Selection Process") +
  xlab("Selection Number") +
  ylab("Within Year Z-Score") +
  fischeR::theme_saf()

ggplot(all_stars_2001_2010, aes(x = year, y = year_divergence)) +
  geom_point() +
  scale_x_continuous(breaks = c(2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
                                2009, 2010)) +
  geom_hline(yintercept = 0) +
  xlab("Year") +
  ylab("Within Year Z-Score") +
  fischeR::theme_saf()

first_all_star <-
  all_stars_2001_2010 %>%
  filter(n_selection == 1)

n_selection_df <-
  all_stars_2001_2010 %>%
  # filter(n_selection >= 2) %>%
  filter(name %in% first_all_star$name) %>%
  group_by(n_selection) %>%
  summarise(Count = n())

# n_selection_df <-
#   rbind(c(1, length(unique(all_stars_2001_2010$name))),
#         n_selection_df)

n_selection_df <-
  n_selection_df %>%
  mutate(standard = ifelse(n_selection == 1, 1, Count / Count[1]),
         conditional = (Count / Count[1]) / (lag(Count) / Count[1]))

n_selection_df <-
  n_selection_df %>%
  tidyr::pivot_longer(-c(n_selection, Count), names_to = "type", 
                      values_to = "prob")

ggplot(n_selection_df, aes(x = n_selection, y = prob, group = type)) +
  geom_point(aes(shape = type), size = 2) +
  geom_line(aes(lty = type)) +
  scale_x_continuous(breaks = c(1:15)) +
  scale_shape_discrete("Type of Probability", solid = FALSE) +
  scale_linetype_discrete("Type of Probability") +
  xlab("Selection Number") +
  ylab("Probability of All Star Selection") +
  labs(caption = "Based on 2001-2010 All-Star Games") +
  fischeR::theme_saf() +
  theme(legend.direction = "horizontal",
        legend.position = "bottom")
